import org.junit.Test;

import static org.junit.Assert.*;

public class FunctionalTest {

    @Test(expected = Exception.class)
    public void testIntegralException() {
        ExponentFunction func = new ExponentFunction(1, 2, 1, 0);
        IntegralFunctional<ExponentFunction> integralFunctional = new IntegralFunctional<>(10, 100);
        assertEquals( 0, integralFunctional.calculate(func), 0.1);
    }

    @Test
    public void testExpIntegral() {
        ExponentFunction func = new ExponentFunction(1, 2, 1, 2);
        IntegralFunctional<ExponentFunction> integralFunctional = new IntegralFunctional<>(1, 2);
        assertEquals( 6.67,integralFunctional.calculate(func), 0.1);
    }

    @Test
    public void testRationalIntegral() {
        RationalFunction func = new RationalFunction(1, 2, 1, 3, 1,5);
        IntegralFunctional<RationalFunction> integralFunctional = new IntegralFunctional<>(1, 2);
        assertEquals( 0.77,integralFunctional.calculate(func), 0.1);
    }

    @Test
    public void testSinIntegral() {
        SinFunction func = new SinFunction(1, 2, 1, 3);
        IntegralFunctional<SinFunction> integralFunctional = new IntegralFunctional<>(1, 2);
        assertEquals( 0.118,integralFunctional.calculate(func), 0.1);
    }

    @Test
    public void testSumFunctionalSin() {
        SinFunction sinFunction = new SinFunction(1, 1, 1, 2);
        SumFunctional<SinFunction> functional = new SumFunctional<>();
        double expected = Math.sin(1)+Math.sin(2)+Math.sin(3./2);
        assertTrue(functional.calculate(sinFunction) - expected < 0.01);
    }

    @Test
    public void testSumFunctionalRational() {
        RationalFunction func = new RationalFunction(1, 1, 1, 1, 1, 10);
        SumFunctional<RationalFunction> functional = new SumFunctional<>();
        assertEquals(3, functional.calculate(func), 0.01);
    }

    @Test
    public void testSumFunctionalExp() {
        ExponentFunction func = new ExponentFunction(1, 1, 1, 1);
        SumFunctional<ExponentFunction> functional = new SumFunctional<>();
        assertEquals(11.154, functional.calculate(func), 0.1);
    }

}