import org.junit.Test;

import static org.junit.Assert.*;

public class FunctionTest {

    @Test
    public void testLinearFunction() {
        LinearFunction func = new LinearFunction(1, 2, 1, 10);
        assertEquals( 4, func.getValue(2), 0.1);
    }

    @Test
    public void testExpFunction() {
        ExponentFunction expFunction = new ExponentFunction(1, 2, 1, 3);
        assertEquals(9.3890, expFunction.getValue(2), 0.001);
    }

    @Test
    public void testSinFunction() {
        SinFunction sinFunction = new SinFunction(1, 5, -1, 1);
        assertEquals(Math.sin(0.5), sinFunction.getValue(0.1), 0.1);
    }

    @Test
    public void testRationalFunction() {
        RationalFunction rationalFunction = new RationalFunction(10, 2, 1,1, 0, 10);
        assertEquals(7.333, rationalFunction.getValue(2), 0.001);
    }

    @Test(expected = Exception.class)
    public void testError() {
        ExponentFunction expFunction = new ExponentFunction(1, 2, 0, 1);
        expFunction.getValue(100);
    }

}