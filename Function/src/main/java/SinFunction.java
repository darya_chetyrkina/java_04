public class SinFunction extends LinearFunction implements IFunction {
//f(x) = Asin(Bx),
    public SinFunction(double A, double B, double inferiorBound, double superiorBound){
        super(A,B,inferiorBound,superiorBound);
    }

    @Override
    public double getValue(double x) {
        if (x<inferiorBound || x > superiorBound) throw new IllegalArgumentException();
        return A*Math.sin(B*x);
    }
}
