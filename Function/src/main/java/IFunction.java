public interface IFunction {
    //Разработать интерфейс для понятия «функция одного вещественного аргумента,
    //определенная на отрезке [a; b]». Интерфейс должен содержать метод вычисления значения
    //функции при заданном аргументе и методы получения границ отрезка.
    double getValue(double x);
    double getInferiorBound();
    double getSuperiorBound();

}