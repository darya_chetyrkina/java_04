public interface IFunctional <T extends IFunction> {
    double calculate(T func);

}
