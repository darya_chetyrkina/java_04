public class SumFunctional <T extends IFunction> implements IFunctional <T>{

    public SumFunctional(){}
    public double calculate(T func){

        return func.getValue(func.getInferiorBound())+
                func.getValue(func.getSuperiorBound())
                + func.getValue((func.getInferiorBound()+ func.getSuperiorBound())/2);
    }
}
