public class LinearFunction implements IFunction {
    //Ax+B
    protected double A;
    protected double B;
    protected double inferiorBound;
    protected double superiorBound;

    public LinearFunction(double A, double B, double inferiorBound, double superiorBound){
        this.A = A;
        this.B = B;
        this.inferiorBound = inferiorBound;
        this.superiorBound = superiorBound;
    }

    @Override
    public double getValue(double x){
        if (x > superiorBound || x < inferiorBound) throw new IllegalArgumentException();
        return A*x+B;
    }

    @Override
    public double getInferiorBound(){
        return A;
    }

    @Override
    public double getSuperiorBound(){
        return B;
    }

}
