public class RationalFunction extends LinearFunction implements IFunction{
    //f(x) = (Ax + B) / (Cx + D)
    private double C;
    private double D;

    public RationalFunction(double A, double B, double C, double D, double inferiorBound, double superiorBound){
        super(A, B, inferiorBound, superiorBound);
        if (C == 0 && D == 0) throw new IllegalArgumentException();
        this.C = C;
        this.D = D;
    }

    public double getValue(double x){
        if (x < inferiorBound || x > superiorBound) throw new IllegalArgumentException();
        double denominator = C*x+D;
        if (denominator == 0) throw new IllegalArgumentException();
        return (A*x+B)/denominator;
    }

}
