public class IntegralFunctional <T extends IFunction> implements IFunctional<T>{
    private final double right, left;
    
    public IntegralFunctional(double left, double right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public double calculate(T func) throws IllegalArgumentException {
        //f(x)dx=(b-a)*f(a+b)/2)
        //if (left < func.getInferiorBound() || right > func.getSuperiorBound()) throw new IllegalArgumentException();
        int n = 64;//количество разбиений
        double h = Math.abs(right-left)/n; //шаг
        double result = 0.0;

        for (int i = 0; i < n; i++) {
            result += func.getValue(left + i * h) * h;

        }
        return result;
    }
}
