public class ExponentFunction extends LinearFunction implements IFunction {
    // f(x) = Aexp(x) + B

    public ExponentFunction(double A, double B, double inferiorBound, double superiorBound){
        super(A, B, inferiorBound, superiorBound);
    }

    @Override
    public double getValue(double x){
        if (x < inferiorBound || x > superiorBound) throw new IllegalArgumentException();
        return A*Math.exp(x)+B;
    }
}
