import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTrinomialsTest {

    @Test(expected = Exception.class)
    public void solve() {
        new SquareTrinomials(0,1,1);
    }

    @Test
    public void solve2(){
        SquareTrinomials sq = new SquareTrinomials(1,1,1);
        assertNull(sq.solve());
    }

    @Test
    public void solve3(){
        SquareTrinomials sq = new SquareTrinomials(1,1,0);
        double[] arr = new double[2];
        arr[0] = -1;
        arr[1] = 0;
        assertArrayEquals(arr, sq.solve(), 0.001);
    }

    @Test
    public void solve4(){
        SquareTrinomials sq = new SquareTrinomials(1,0,1);
        assertNull(sq.solve());
    }

}