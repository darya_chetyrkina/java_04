public class SquareTrinomials {

    private final double a;
    private final double b;
    private final double c;

    public SquareTrinomials(double a, double b, double c){
        if (a == 0) throw new IllegalArgumentException("This is not square equation!");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double[] solve(){
        //возвращает null если нет корней

        double D = (b*b-4*a*c); //дискриминант
        double[] roots = null; //корни

        if (D == 0){
             roots = new double[1];
             roots[0] =  (-b-Math.sqrt(D))/(2*a);
        }else if (D > 0){
            roots = new double[2];
            roots[0] = (-b-Math.sqrt(D))/(2*a);
            roots[1] = (-b+Math.sqrt(D))/(2*a);
        }

        return roots;
    }
}
