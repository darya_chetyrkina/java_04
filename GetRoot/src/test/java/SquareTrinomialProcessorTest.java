import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTrinomialProcessorTest {

    @Test (expected = Exception.class)
    public void getBiggerRootExceptionTest() {
        SquareTrinomials sq = new SquareTrinomials(1,1,1);
        SquareTrinomialProcessor.getBiggerRoot(sq.solve());
    }

    @Test
    public void getBiggerRoot(){
        SquareTrinomials sq = new SquareTrinomials(1,1,0);
        assertTrue(SquareTrinomialProcessor.getBiggerRoot(sq.solve()) < 0.001);
    }
}