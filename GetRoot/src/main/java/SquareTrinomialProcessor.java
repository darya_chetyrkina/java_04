public class SquareTrinomialProcessor {

    public static double getBiggerRoot(double[] roots){
        if (roots == null) throw new IllegalArgumentException("Корней нет.");
        double maxRoot = roots[0];
        for (double root: roots) {
            if (maxRoot < root){
                maxRoot = root;
            }
        }
        return maxRoot;
    }
}
